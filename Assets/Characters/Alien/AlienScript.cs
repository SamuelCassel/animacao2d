﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienScript : MonoBehaviour
{
    private Rigidbody2D body;
    public float jumpForce;

    private Animator animator;
    void Start()
    {
        this.body = GetComponent<Rigidbody2D>();
        this.body.isKinematic = true;
        this.animator = GetComponent<Animator>();

        this.animator.enabled = false;
    }
    void Update()
    {
        if (Input.GetButton("Fire1"))
        {
            this.body.isKinematic = false;
            this.body.AddForce(new Vector2(0, this.jumpForce));
            this.animator.enabled = true;
        }

        this.transform.rotation = Quaternion.Euler(0, 0, body.velocity.y * 3);
    }
}
